Steer Try Design {#steer_try-package-design}
===========

# Purpose 
Steer Try package aims to reach send lgsvl messages through to lgsvl simulator. Which uses Ros2 nodes as communication method.
To use this package you have to add lgsvl control messages to your node.
<!-- Required -->
<!-- Things to consider:
    - Why did we implement this feature? -->
# Design
Steer Try package designed to send lgsvl control messages.
You have to be carefull about node topic while transmitting the lgsvl control messages.
The Steer Try node works with lgsvl mcontrol messages. The node takes time dependent sine values from 
Sin(w.t) function and returns a number between -1 ... 1  interval.
<!-- Required -->
<!-- Things to consider:
    - How does it work? -->


## Assumptions / Known limits
Output from lgsvl target_wheel_angle message will be between -1 to 1 interval.


## Inputs / Outputs / API
Steer try node takes input from rclcpp timerbase. It takes the time information from timerbase every 100 ms. 
The node publish it's message with a given 10 Hz frequency because of its 100 ms callback function.
<!-- Required -->
<!-- Things to consider:
    - How do you use the package / API? -->


## Inner-workings / Algorithms
The algorithm inside of the code is a basic sinusoidal value calculator.
It takes values as radians and outputs due to sine function.
<!-- If applicable -->


## Error detection and handling
There is no error detection exist in the code.
<!-- Required -->


# Security considerations
<!-- Required -->
<!-- Things to consider:
- Spoofing (How do you check for and handle fake input?)
- Tampering (How do you check for and handle tampered input?)
- Repudiation (How are you affected by the actions of external actors?).
- Information Disclosure (Can data leak?).
- Denial of Service (How do you handle spamming?).
- Elevation of Privilege (Do you need to change permission levels during execution?) -->


# References / External links
LGSVL control messages library:
-> https://github.com/lgsvl/lgsvl_msgs

# Related issues

