// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.

#include <algorithm>
#include <deque>
#include <limits>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include "steer_try/pid.hpp"
#include "steer_try/steer_try_node.hpp"
#include "rclcpp_components/register_node_macro.hpp"




namespace autoware
{
namespace steer_try
{

using namespace std::chrono_literals;   
using namespace std;

SteerTryNode::SteerTryNode(const rclcpp::NodeOptions & options)
:  Node("steer_try", options)
{
  
  publisher_ = 
  this->create_publisher<lgsvl_msgs::msg::VehicleControlData>("/lgsvl/vehicle_control_cmd", 10);

  Time_=
  this->create_wall_timer(100ms, std::bind(&SteerTryNode::ElapsedTime, this));

  timer_ =
  this->create_wall_timer(100ms, std::bind(&SteerTryNode::timer_callback, this));
  
  subscription_ = 
  this->create_subscription<lgsvl_msgs::msg::CanBusData>("/lgsvl/state_report"
         ,10, std::bind(&SteerTryNode::topic_callback, this, _1));

}

float64_t SteerTryNode::ElapsedTime()
  {
      auto t_start = m_time_prev;
      auto t_end = m_clock->now();  
      duration[0] = (t_end - t_start).seconds();
      duration[2] = duration[0] - duration[1];
      duration[1] = duration[0];
      return(duration[2]);
  }

PID pid = PID( 100.0, -100.0, 20.0, 12.0, 0.22);

void SteerTryNode::topic_callback(const lgsvl_msgs::msg::CanBusData::SharedPtr ComingMsg)
{
  //RCLCPP_INFO(this->get_logger(), "Speed is: '%f", msg.speed_mps);
  //cout << ComingMsg->speed_mps << "\n";
    
  ComingSpeed = ComingMsg->speed_mps;
  
      
  //cout<< "Coming Speed:"<<fixed<<setprecision(20)<<ComingMsg->speed_mps<<"going:"<<fixed<<setprecision(20)<<ControllerOut<<endl;
        
}


void SteerTryNode::timer_callback()
    { 
      ElapsedTime_ = (ElapsedTime()*10000.0);
      counter = counter +1;
      if (counter < 100)
      {
      ControllerOut = pid.calculate( ElapsedTime_,15.0, ComingSpeed);    
      }
      else
      {
      ControllerOut = pid.calculate( ElapsedTime_,0.0, ComingSpeed);    
      }


      if ( ControllerOut > 0 )
      {
        BrakePedalPID = 0.0;
        GasPedalPID = ControllerOut/100;
      }else
      {
        GasPedalPID = 0.0;
        BrakePedalPID = abs(ControllerOut/100);
      }

      cout<< "duration:"<<fixed<<setprecision(20)<<ElapsedTime_<<endl;
      //msg.target_wheel_angle = static_cast<float32_t>(sin(M_PI/180.0*ElapsedTime()));

      msg.acceleration_pct = static_cast<float32_t>(GasPedalPID);
      msg.braking_pct =  static_cast<float32_t>(BrakePedalPID);

      publisher_->publish(msg);
    }
}  // namespace steer_try
}  // namespace autoware

// This acts as an entry point, allowing the component to be
// discoverable when its library is being loaded into a running process
RCLCPP_COMPONENTS_REGISTER_NODE(autoware::steer_try::SteerTryNode)
