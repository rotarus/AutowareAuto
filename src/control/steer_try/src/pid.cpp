/**
 * Copyright 2019 Bradley J. Snyder <snyder.bradleyj@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//#ifndef _PID_SOURCE_
//#define _PID_SOURCE_

#include <iostream>
#include <cmath>
#include "steer_try/pid.hpp"

namespace autoware
{
namespace steer_try
{



using namespace std;

class PIDImpl
{
    public:
        PIDImpl( float64_t max, float64_t min, float64_t Kp, float64_t Kd, float64_t Ki );
        ~PIDImpl();
        float64_t calculate(float64_t ElapsedTime, float64_t setpoint, float64_t pv );

    private:
        float64_t _max;
        float64_t _min;
        float64_t _Kp;
        float64_t _Kd;
        float64_t _Ki;
        float64_t _pre_error;
        float64_t _integral;
};


PID::PID( float64_t max, float64_t min, float64_t Kp, float64_t Kd, float64_t Ki )
{
    pimpl = new PIDImpl(max,min,Kp,Kd,Ki);
}
float64_t PID::calculate(float64_t ElapsedTime, float64_t setpoint, float64_t pv )
{
    return pimpl->calculate(ElapsedTime,setpoint,pv);
}
PID::~PID() 
{
    delete pimpl;
}


/**
 * Implementation
 */
PIDImpl::PIDImpl( float64_t max, float64_t min, float64_t Kp, float64_t Kd, float64_t Ki ) :
    _max(max),
    _min(min),
    _Kp(Kp),
    _Kd(Kd),
    _Ki(Ki),
    _pre_error(0),
    _integral(0)
{
}

float64_t PIDImpl::calculate( float64_t ElapsedTime, float64_t setpoint, float64_t pv )
{
    
    // Calculate error
    float64_t error = setpoint - pv;

    // Proportional term
    float64_t Pout = _Kp * error;

    // Integral term
    _integral += error * ElapsedTime;
    float64_t Iout = _Ki * _integral;

    // Derivative term
    float64_t derivative = (error - _pre_error) / ElapsedTime;
    float64_t Dout = _Kd * derivative;

    // Calculate total output
    float64_t output = Pout + Iout + Dout;

    // Restrict to max/min
    if( output > _max )
        output = _max;
    else if( output < _min )
        output = _min;

    // Save error to previous error
    _pre_error = error;

    return output;
}

PIDImpl::~PIDImpl()
{
}

}  // namespace steer_try
}  // namespace autoware

//#endif
