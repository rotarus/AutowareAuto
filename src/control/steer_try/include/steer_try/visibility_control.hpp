// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.

#ifndef STEER_TRY__VISIBILITY_CONTROL_HPP_
#define STEER_TRY__VISIBILITY_CONTROL_HPP_

////////////////////////////////////////////////////////////////////////////////
#if defined(__WIN32)
  #if defined(STEER_TRY_BUILDING_DLL) || defined(STEER_TRY_EXPORTS)
    #define STEER_TRY_PUBLIC __declspec(dllexport)
    #define STEER_TRY_LOCAL
  #else  // defined(STEER_TRY_BUILDING_DLL) || defined(STEER_TRY_EXPORTS)
    #define STEER_TRY_PUBLIC __declspec(dllimport)
    #define STEER_TRY_LOCAL
  #endif  // defined(STEER_TRY_BUILDING_DLL) || defined(STEER_TRY_EXPORTS)
#elif defined(__linux__)
  #define STEER_TRY_PUBLIC __attribute__((visibility("default")))
  #define STEER_TRY_LOCAL __attribute__((visibility("hidden")))
#elif defined(__APPLE__)
  #define STEER_TRY_PUBLIC __attribute__((visibility("default")))
  #define STEER_TRY_LOCAL __attribute__((visibility("hidden")))
#else
  #error "Unsupported Build Configuration"
#endif

#endif  // STEER_TRY__VISIBILITY_CONTROL_HPP_
